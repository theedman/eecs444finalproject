from abc import ABC, abstractmethod

class resource(ABC):
    @abstractmethod
    def create(self, *args):
        pass

    @abstractmethod
    def delete(self, *args):
        pass