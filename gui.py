import pyforms
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlList, ControlButton, ControlCombo

from BellLaPadula import BellLaPadula
from BIBA import BIBA
from ACM import ACM

class gui(BaseWidget):
    """
    This applications is a GUI implementation of the People class
    """

    def __init__(self):
        BaseWidget.__init__(self,'Team 5 Final Project')

        #Definition of the forms fields
        '''
        self._userList    = ControlList('People',
            plusFunction    = self.__addPersonBtnAction,
            minusFunction   = self.__rmPersonBtnAction)
        '''
        self.users = []
        self.files = []

        self.models = ['Access Control Matrix', 'Bell-LaPadula', 'BIBA' ]
        self.loadModelButton = ControlButton('Load Model')
        self.loadModelButton.value = self.loadModel
        
        self.curModel = None

        self.addPersonButton = ControlButton('Add')
        self.addPersonButton.value = self.addPerson
        self.editPersonButton = ControlButton('Edit')
        self.editPersonButton.value = self.editPerson
        
        self.addFileButton = ControlButton('Add')
        self.addFileButton.value = self.addFile
        self.editFileButton = ControlButton('Edit')
        self.editFileButton.value = self.editFile
        # self.delFileButton = ControlButton('Delete')
        # self.delFileButton.value = self.delFile

        self.userList = ControlList(readonly=True)
        self.fileList = ControlList(readonly=True)
        self.userList.horizontal_headers = ['Username']
        self.fileList.horizontal_headers = ['Filename']

        self.readButton = ControlButton('Read')
        self.readButton.value = self.read
        self.writeButton = ControlButton('Write')
        self.writeButton.value = self.write
        self.executeButton = ControlButton('Execute')
        self.executeButton.value = self.execute

        self.modelCombo = ControlCombo(label='Active Model')
        for model in self.models:
            self.modelCombo.add_item(model)

        self._formset = ['', '||', '',
                         ('modelCombo', 'loadModelButton'),
                         (('userList', '=', ('addPersonButton', 'editPersonButton')),
            ('readButton','=', 'writeButton','=', 'executeButton'), # '=' indicates a vertical splitter
             ('fileList', '=', ('addFileButton', 'editFileButton'))),
                         '', '||', ''
                         ]

    def loadModel(self):
        if self.curModel is not None:
            self.curModel.close()
            self.users = []
            self.files = []
        if self.modelCombo.current_index == 0:
            print('selecting ACM')
            #self.curModel = #TODO: Make ACM
            self.curModel = ACM(self)
            self.curModel.show()
        elif self.modelCombo.current_index == 1:
            print('selecting BLP')
            self.curModel = BellLaPadula(self)
            self.curModel.show()
        elif self.modelCombo.current_index == 2:
            print('selecting BIBA')
            self.curModel = BIBA(self)
            self.curModel.show()
        self.refresh_table()

    def addPerson(self):
        if self.curModel is not None:
            self.curModel.create_user()
        else:
            self.alert('No Model Loaded!', title='Add Person')

    def editPerson(self):
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            if user_index is not None:
                username = self.userList.value[user_index][0]
                self.curModel.edit_user(username)
            else:
                self.alert('User must be selected!')
        else:
            self.alert('No Model Loaded!', title='Edit Person')

    def addFile(self): # this needs to be abstracted in model.py, doenst ACM need an owner?
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            if user_index is not None:
                username = self.userList.value[user_index][0]
                self.curModel.create_file(username)
            else:
                self.alert('User must be selected!')
        else:
            self.alert('No Model Loaded!', title='Add File')

    def editFile(self): # this is an extension of write, but the model needs to be able to handle this case specifically.
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            file_index = self.fileList.selected_row_index
            if user_index is not None and file_index is not None:
                username = self.userList.value[user_index][0]
                filename = self.fileList.value[file_index][0]
                self.curModel.edit_file(username, filename)
            else:
                self.alert('User and File must be selected!')
        else:
            self.alert('No Model Loaded!', title='Edit File')

    # def delFile(self): # need to talk about this one too, model has delete defined, but for what?
    #     if self.curModel is not None:
    #         print('del file')
    #     else:
    #         self.alert('No Model Loaded!', title='Delete File')

    def read(self):
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            file_index = self.fileList.selected_row_index
            if user_index is not None and file_index is not None:
                username = self.userList.value[user_index][0]
                filename = self.fileList.value[file_index][0]
                self.curModel.read(username, filename)
            else:
                self.alert('User and File must be selected!')
        else:
            self.alert('No Model Loaded!', title='Read File')

    def write(self):
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            file_index = self.fileList.selected_row_index
            if user_index is not None and file_index is not None:
                username = self.userList.value[user_index][0]
                filename = self.fileList.value[file_index][0]
                self.curModel.write(username, filename)
            else:
                self.alert('User and File must be selected!')
        else:
            self.alert('No Model Loaded!', title='Write File')

    def execute(self):
        if self.curModel is not None:
            user_index = self.userList.selected_row_index
            file_index = self.fileList.selected_row_index
            if user_index is not None and file_index is not None:
                username = self.userList.value[user_index][0]
                filename = self.fileList.value[file_index][0]
                self.curModel.execute(username, filename)
            else:
                self.alert('User and File must be selected!')
        else:
            self.alert('No Model Loaded!', title='Execute File')

    def refresh_table(self):
        self.userList.clear()
        self.fileList.clear()

        self.users = self.curModel.get_users()
        self.files = self.curModel.get_files()
        
        for user in self.users:
            self.userList.__add__([user])
        for file in self.files:
            self.fileList.__add__([file])

#Execute the application
if __name__ == "__main__":
    pyforms.start_app(gui, geometry=(200, 200, 800, 400))
