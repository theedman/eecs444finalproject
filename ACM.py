import pyforms

from model import BaseModel
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlList, ControlCombo, ControlButton, ControlText, ControlTextArea

import numpy as np
import pandas as pd


class ACM(BaseWidget):

    def __init__(self, gui):
        super(ACM, self).__init__('Access Control Matrix')
        self.gui = gui
        
        self.acl = ControlList('Access Control Lists')

        self.df = pd.DataFrame()

        self.files = []

        self.acl = ControlList(readonly=True) #readonly?
        #self.acl.horizontal_headers = 'Username'

    
    def refresh_table(self):
        #print(self.df)
        self.acl.clear()

        #self.acl.__add__(self.get_users())
        self.acl.horizontal_headers = ['File Name'] + self.get_users()

        for index, row in self.df.iterrows():
            print('row:', list(row))
            self.acl.__add__([index] + list(row))

        self.gui.refresh_table()

    def create_user(self):
        win = CreateUserWindow(self)
        win.show()

    def edit_user(self, username):
        self.alert("Nothing to edit!")

    def create_file(self, username):
        win = CreateFileWindow(self, username)
        win.show()

    def edit_file(self, username, filename):
        win = FilePerms(self, filename, self.df.loc[filename, username])
        win.show()

    def read(self, username, file):
        win = EditFileWindow(self, username, file, self.df.loc[file, username])
        win.show()

    def write(self, username, file):
        win = EditFileWindow(self, username, file, self.df.loc[file, username])
        win.show()

    def execute(self, username, file):
        perms = self.df.loc[file, username]
        if 'x' in perms:
            self.success('File Successfully Executed')
        else:
            self.alert('No Permissions') 

    def get_users(self):
        return list(self.df)

    def get_files(self):
        return list(self.df.index)

class FilePerms(BaseWidget):
    def __init__(self, model, filename, perms):
        super(FilePerms, self).__init__(filename + ' Permissions')
        self.model = model
        self.o = 'o' in perms
        self.filename = filename

        self.files = ControlList(readonly=not self.o)

        self.files.horizontal_headers = self.model.get_users()

        self.files.__add__(self.model.df.loc[filename])
        #for user in self.model.get_users():


        # for index, row in self.model.df.iterrows():
        #     self.files.__add__([index] + list(row))
        
        self.button = ControlButton('Edit File', enabled = self.o)
        self.button.value = self.edit

        self._formset = ['', '||', '',
                        'files',
                        'button',
                        '', '||', '']

    def edit(self):
        #self.model.df.loc[self._filename.value] = ''
        #self.model.refresh_table()
        owners = 0
        for value in self.files.value[0]:
            if 'o' in value:
                owners += 1
        if owners == 1:
            self.model.df.loc[self.filename] = self.files.value[0]
            self.close()
        else:
            self.alert('Incorrect number of file owners!')

class EditFileWindow(BaseWidget):
    def __init__(self, model, username, filename, perms):
        super(EditFileWindow, self).__init__(filename)
        self.model = model
        self.username = username
        self.r = 'r' in perms
        self.w = 'w' in perms

        for idx, file_dict in enumerate(self.model.files):
            if file_dict['name'] == filename:
                self.content_idx = idx

        # Definition of the forms fields
        #self._filename = ControlText('File Name', 'Default value', readonly)
        self._content = ControlTextArea(readonly=not self.w)
        if(self.r):
            self._content.value = self.model.files[self.content_idx]['content']

        self._button = ControlButton('Edit File', enabled=self.w)

        self._formset = ['', '||', '',
                         '_content',
                          '_button',
                         '', '||', '']

        self._button.value = self.edit

    def edit(self):
        self.model.files[self.content_idx]['content'] = self._content.value
        self.model.refresh_table()
        self.close()

class CreateFileWindow(BaseWidget):
    def __init__(self, model, username):
        super(CreateFileWindow, self).__init__('Create File')
        self.model = model
        # Definition of the forms fields
        self._filename = ControlText('File Name', 'Default value')
        self._content = ControlTextArea()
        self._button = ControlButton('Create File')

        self._formset = ['', '||', '',
                         '_filename', 
                         '_content',
                         '_button',
                         '', '||', '']

        self._button.value = self.create_file_action

        self.username = username

    def create_file_action(self):
        self.model.df.loc[self._filename.value] = ''
        self.model.df.loc[self._filename.value, self.username] = 'rwxo'
        self.model.refresh_table()
        self.model.files.append({'name':self._filename.value, 'content':self._content.value})
        self.close()

class CreateUserWindow(BaseWidget):
    def __init__(self, model):
        super(CreateUserWindow, self).__init__('Create User')
        self.model = model
        # Definition of the forms fields
        self._username = ControlText('Username', 'Default value')
        self._button = ControlButton('Create User')

        self._button.value = self.create_user_action

    def create_user_action(self):
        self.model.df[self._username.value] = ''
        self.model.refresh_table()
        self.close()

