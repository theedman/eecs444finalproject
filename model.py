import abc

#this should extend something
from pyforms.basewidget import BaseWidget

class BaseModel(abc.ABC):

    @abc.abstractmethod
    def create_user(self):
        raise NotImplementedError()
    
    @abc.abstractmethod
    def edit_user(self, username):
        raise NotImplementedError()

    @abc.abstractmethod
    def create_file(self, username):
        raise NotImplementedError()

    @abc.abstractmethod
    def edit_file(self, username, file):
        raise NotImplementedError()
    
    @abc.abstractmethod
    def read(self, username, file):
        raise NotImplementedError()
    
    @abc.abstractmethod
    def write(self, username, file):
        raise NotImplementedError()
   
    @abc.abstractmethod
    def execute(self, username, file):
        raise NotImplementedError()
    
    # @abc.abstractmethod
    # def delete(self, username, file):
    #     raise NotImplementedError()
    
    @abc.abstractmethod
    def get_users(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_files(self):
        raise NotImplementedError()
    
    @abc.abstractmethod
    def display_table(self):
        #hopefully this will just be called once, and we can dynamically modify the table without having to call
        # display table over and over.
        raise NotImplementedError()

    
    


    