import pyforms

from model import BaseModel
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlList, ControlCombo
from pyforms.controls import ControlButton
from pyforms.controls import ControlText, ControlTextArea

# from pymsgbox import alert


class BIBA(BaseWidget):

    def __init__(self, gui):
        super(BIBA, self).__init__('BIBA')
        self.gui = gui

        self.user_list = ControlList('Users', readonly=True)
        self.user_list.horizontal_headers = ['Username', 'Integrity Level']

        self.file_list = ControlList('Files', readonly=True)
        self.file_list.horizontal_headers = ['Filename', 'Integrity Level']

        self._send_message_button = ControlButton('Send Message(Invoke)')
        self._send_message_button.value = self._send_message

        self.integrity_levels = {
            'High Integrity': 4,
            'Some Integrity': 3,
            'Low Integrity': 2,
        }

        self.users = []
        self.user_integrity = {}

        self.files = []
        self.file_integrity = {}
        self.file_contents = {}

        self._formset = ['', '||', '',
                         ('user_list', 'file_list'),
                         '_send_message_button',
                         '', '||', ''
                         ]

        ## CODE Used for testing. should be remove once UI is done.
        self._create = ControlButton('Create User')
        self._create.value = self.create_user

        self._edit_button = ControlButton('Edit User')
        self._edit_button.value = self._edit_user

        self._create_file_button = ControlButton('Create File')
        self._create_file_button.value = self.create_file

        self._read_button = ControlButton('Read')
        self._read_button.value = self._read

        self._write_button = ControlButton('Write')
        self._write_button.value = self._write

        self._send_msg_button = ControlButton('Send Message')
        self._send_msg_button.value = self._send_message

    def create_user(self):
        win = CreateUserWindow(self)
        win.show()

    def edit_user(self, username):
        # win = EditUserWindow(self, username)
        # win.show()
        self.alert("User Integrity cannot be editted")  # should not change integrity level

    def edit_file(self, user, file):
        self.alert("File Integrity cannot be editted")

    # TEST CODE SHOULD BE REMOVED AFTER MAIN UI IS DONE
    def _edit_user(self):
        # get first selected user
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]
        self.edit_user(username)

    def create_file(self, username):
        win = CreateFileWindow(self, username)
        win.show()

    def edit_file(self, username, filename):
        self.alert('File security cannot be edited')

    def read(self, username, file):
        file_integrity = self.integrity_levels[self.file_integrity[file]]
        user_integrity = self.integrity_levels[self.user_integrity[username]]

        if user_integrity <= file_integrity:
            ShowFileWindow(self, file, write=False).show()
        else:
            self.alert('User Can Not Read This File')

    def _read(self):
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]

        indexes = self.file_list.selected_rows_indexes
        filename = self.file_list.value[indexes[0]][0]
        self.read(username, filename)

    def write(self, username, file):
        file_integrity = self.integrity_levels[self.file_integrity[file]]
        user_integrity = self.integrity_levels[self.user_integrity[username]]

        if user_integrity >= file_integrity:
            ShowFileWindow(self, file, write=True).show()
        else:
            self.alert('User Can Not Read This File')

    def _write(self):
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]

        indexes = self.file_list.selected_rows_indexes
        filename = self.file_list.value[indexes[0]][0]
        self.write(username, filename)

    def send_message(self, username):
        win = SendMessageWindow(self, username)
        win.show()

    def _send_message(self):
        indexes = self.user_list.selected_rows_indexes
        if indexes != []:
            username = self.user_list.value[indexes[0]][0]
            self.send_message(username=username)
        else:
            self.alert('Must select a sender!')

    def execute(self, username, file):
        file_integrity = self.integrity_levels[self.file_integrity[file]]
        user_integrity = self.integrity_levels[self.user_integrity[username]]

        if user_integrity <= file_integrity:
            self.success('User Can Execute This File')
        else:
            self.alert('User Can Not Execute This File')


    def delete(self, username, file):
        pass

    def get_users(self):
        return self.users

    def get_files(self):
        return self.files

    def display_table(self):
        pass

    def add_user_action(self, name, integrity_level):
        self.users.append(name)
        self.user_integrity[name] = integrity_level
        self.refresh_table()

    def add_file_action(self, name, integrity_level, content):
        self.files.append(name)
        self.file_integrity[name] = integrity_level
        self.file_contents[name] = content
        self.refresh_table()

    # def edit_user_action(self, name, integrity_level):
    #     self.user_integrity[name] = integrity_level
    #     self.refresh_table()

    def send_message_action(self, sender, receiver):
        sender_integrity = self.integrity_levels[self.user_integrity[sender]]
        receiver_integrity = self.integrity_levels[self.user_integrity[receiver]]

        if sender_integrity >= receiver_integrity:
            self.success('{} Can Send Message To {}'.format(sender, receiver))
        else:
            self.alert('{} Can Not Send Message To {}'.format(sender, receiver))

    def refresh_table(self):
        self.gui.refresh_table()
        self.user_list.clear()
        for user in self.users:
            self.user_list.__add__([user, self.user_integrity[user]])

        self.file_list.clear()
        for file in self.files:
            self.file_list.__add__([file, self.file_integrity[file]])


class CreateUserWindow(BaseWidget):
    def __init__(self, model):
        super(CreateUserWindow, self).__init__('Create User')
        self.model = model
        # Definition of the forms fields
        self._username = ControlText('Username', 'Default value')
        self._integrity_levels = ControlCombo(label='Integrity Level')
        self._button = ControlButton('Create User')

        self._button.value = self.create_user_action

        self._formset = ['', '||', '',
                         '_username', '_integrity_levels', '_button',
                         '', '||', '']

        for level in model.integrity_levels:
            self._integrity_levels.add_item(level)

    def create_user_action(self):
        self.model.add_user_action(self._username.value, self._integrity_levels.value)
        self.close()


class CreateFileWindow(BaseWidget):
    def __init__(self, model, user):
        super(CreateFileWindow, self).__init__('Create File')
        self.model = model
        # Definition of the forms fields
        self._filename = ControlText('File Name', 'Default value')
        self._content = ControlTextArea()
        self._integrity_levels = ControlCombo(label='Integrity Level')
        self._button = ControlButton('Create File')

        self._formset = ['', '||', '',
                         '_filename', '_content', '_integrity_levels', '_button',
                         '', '||', '']

        self._button.value = self.create_file_action

        self.user = user

        for level in model.integrity_levels:
            self._integrity_levels.add_item(level)

    def create_file_action(self):
        file_integrity = self.model.integrity_levels[self._integrity_levels.value]
        user_integrity = self.model.integrity_levels[self.model.user_integrity[self.user]]

        if user_integrity >= file_integrity:
            self.model.add_file_action(self._filename.value, self._integrity_levels.value, self._content.value)
            self.close()
        else:
            self.alert('File integrity is too high')

class EditUserWindow(BaseWidget):
    def __init__(self, model, username):
        self.username = username
        super(EditUserWindow, self).__init__('Edit {}'.format(username))
        self.model = model
        # Definition of the forms fields
        self._integrity_levels = ControlCombo(label='Integrity Level')
        self._button = ControlButton('Edit User')

        self._button.value = self.edit_user_action

        for level in model.security_levels:
            self._integrity_levels.add_item(level)

    def edit_user_action(self):
        self.model.edit_user_action(self.username, self._integrity_levels.value)
        self.close()


class SendMessageWindow(BaseWidget):
    def __init__(self, model, username):
        self.username = username
        super(SendMessageWindow, self).__init__('Sending Message: {}'.format(username))
        self.model = model

        self._user_list = ControlList(label='Choose a Receiver', readonly=True)

        self._send_button = ControlButton('Send')
        self._send_button.value = self.send_message_action

        self._close_button = ControlButton('Close')
        self._close_button.value = self.close

        self._formset = ['', '||', '', '_user_list', ('_send_button', '_close_button'), '', '||', '']

        for user in model.get_users():
            self._user_list.__add__([user])

    def send_message_action(self):
        receiver = self._user_list.get_currentrow_value()
        if receiver != []:
            self.model.send_message_action(self.username, receiver[0])
        else:
            self.alert('Must select a receiver!')


class ShowFileWindow(BaseWidget):
    def __init__(self, model, filename, write: bool):
        self._filename = filename
        super(ShowFileWindow, self).__init__(filename)
        self.model = model

        self._content = ControlTextArea(readonly=not write)
        if write:
            self._content.value = ''
        else:
            self._content.value = self.model.file_contents[filename]

        self._save_button = ControlButton('Save', enabled=write)
        self._save_button.value = self.save

        self._cancel_button = ControlButton('Cancel')
        self._cancel_button.value = self.close

        self._formset = ['', '||', '',
                         '_content',
                         ('_save_button', '_cancel_button'),
                         '', '||', '']

    def save(self):
        self.model.file_contents[self._filename] = self._content.value
        self.close()


if __name__ == '__main__':
    pyforms.start_app(BIBA)
