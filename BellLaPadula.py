import pyforms

from model import BaseModel
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlList, ControlCombo
from pyforms.controls import ControlButton
from pyforms.controls import ControlText, ControlTextArea

# from pymsgbox import alert


class BellLaPadula(BaseWidget):

    def __init__(self, gui):
        super(BellLaPadula, self).__init__('Bell-LaPadula')
        self.gui = gui

        self.user_list = ControlList('Users', readonly=True)
        self.user_list.horizontal_headers = ['Username', 'Category', 'Security Level']

        self.file_list = ControlList('Files', readonly=True)
        self.file_list.horizontal_headers = ['Filename', 'Category', 'Security Level']

        self.security_levels = {
            'Top Secret': 4,
            'Secret': 3,
            'Confidential': 2,
            'Unclassified': 1
        }
        self.categories = ['nuclear', 'crypto', 'finance']
        self.users = []
        self.user_security = {}
        self.user_categories = {}

        self.files = []
        self.file_security = {}
        self.file_categories = {}
        self.file_contents = {}

        self._formset = ['', '||', '',
                         ('user_list', 'file_list'),
                         #('_create','_edit_button','_create_file_button', '_read_button','_write_button')
                         '', '||', ''
                         ]

        ## CODE Used for testing. should be remove once UI is done.
        self._create = ControlButton('Create User')
        self._create.value = self.create_user

        self._edit_button = ControlButton('Edit User')
        self._edit_button.value = self._edit_user

        self._create_file_button = ControlButton('Create File')
        self._create_file_button.value = self.create_file

        self._read_button = ControlButton('Read')
        self._read_button.value = self._read

        self._write_button = ControlButton('Write')
        self._write_button.value = self._write

    def create_user(self):
        win = CreateUserWindow(self)
        win.show()

    def edit_user(self, username):
        win = EditUserWindow(self, username)
        win.show()

    #TEST CODE SHOULD BE REMOVED AFTER MAIN UI IS DONE
    def _edit_user(self):
        # get first selected user
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]
        self.edit_user(username)

    def create_file(self, user):
        win = CreateFileWindow(self, user)
        win.show()

    def read(self, username, file):
        file_categories = self.file_categories[file]
        user_categories = self.user_categories[username]

        file_security = self.security_levels[self.file_security[file]]
        user_security = self.security_levels[self.user_security[username]]

        #file_categories is a subset of user_categires
        if user_security >= file_security and all(c in user_categories for c in file_categories):
            ShowFileWindow(self, file, write=False).show()
        elif user_security < file_security:
            self.alert('User security is too low')
        else:
            self.alert('File categories is not a subset of user categories')

    def _read(self):
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]

        indexes = self.file_list.selected_rows_indexes
        filename = self.file_list.value[indexes[0]][0]
        self.read(username, filename)

    def write(self, username, file):
        file_categories = self.file_categories[file]
        user_categories = self.user_categories[username]

        file_security = self.security_levels[self.file_security[file]]
        user_security = self.security_levels[self.user_security[username]]

        if user_security <= file_security and all(c in file_categories for c in user_categories):
            ShowFileWindow(self, file, write=True).show()
        elif user_security > file_security:
            self.alert('File security is too low')
        else:
            self.alert('File categories is not a superset of user categories')

    def _write(self):
        indexes = self.user_list.selected_rows_indexes
        username = self.user_list.value[indexes[0]][0]

        indexes = self.file_list.selected_rows_indexes
        filename = self.file_list.value[indexes[0]][0]
        self.write(username, filename)

    def execute(self, username, file):
        file_categories = self.file_categories[file]
        user_categories = self.user_categories[username]

        file_security = self.security_levels[self.file_security[file]]
        user_security = self.security_levels[self.user_security[username]]

        if user_security >= file_security and all(c in user_categories for c in file_categories):
            self.success('User Can Execute This File')
        elif user_security < file_security:
            self.alert('User security is too low')
        else:
            self.alert('File categories is not a subset of user categories')

    def delete(self, username, file):
        pass

    def get_users(self):
        print('users', self.users)
        return self.users
        #pass

    def get_files(self):
        return self.files
        #pass

    def display_table(self):
        pass

    def add_user_action(self, name, categories, security_level):
        self.users.append(name)
        self.user_categories[name] = categories
        self.user_security[name] = security_level
        self.refresh_table()

    def add_file_action(self, name, categories, security_level, content):
        self.files.append(name)
        self.file_categories[name] = categories
        self.file_security[name] = security_level
        self.file_contents[name] = content
        self.refresh_table()

    def edit_user_action(self, name, category, security_level):
        self.user_categories[name] = category
        self.user_security[name] = security_level
        self.refresh_table()

    def edit_file(self, username, filename):
        self.alert('File Integrity cannot be edited')

    def refresh_table(self):
        self.gui.refresh_table()
        self.user_list.clear()
        for user in self.users:
            cats = ",".join(self.user_categories[user])
            self.user_list.__add__([user, cats, self.user_security[user]])

        self.file_list.clear()
        for file in self.files:
            cats = ",".join(self.file_categories[file])
            self.file_list.__add__([file, cats, self.file_security[file]])


class CreateUserWindow(BaseWidget):
    def __init__(self, model):
        super(CreateUserWindow, self).__init__('Create User')
        self.model = model
        # Definition of the forms fields
        self._username = ControlText('Username', 'Default value')
        self._categories = ControlList(label='Categories', readonly=True)
        self._security_levels = ControlCombo(label='Security Level')
        self._button = ControlButton('Create User')

        self._button.value = self.create_user_action
        for c in model.categories:
            self._categories.__add__([c])

        for level in model.security_levels:
            self._security_levels.add_item(level)

    def create_user_action(self):
        indexes = self._categories.selected_rows_indexes
        cats = []
        for index in indexes:
            cats.append(self._categories.value[index][0])
        self.model.add_user_action(self._username.value, cats, self._security_levels.value)
        self.close()

class CreateFileWindow(BaseWidget):
    def __init__(self, model, user):
        super(CreateFileWindow, self).__init__('Create File')
        self.model = model
        self.user = user
        # Definition of the forms fields
        self._filename = ControlText('File Name', 'Default value')
        self._categories = ControlList(label='Categories', readonly=True)
        self._content = ControlTextArea()
        self._security_levels = ControlCombo(label='Security Level')
        self._button = ControlButton('Create File')

        self._formset = ['', '||', '',
                         '_filename', '_content', '_categories', '_security_levels', '_button',
                         '', '||', '']

        self._button.value = self.create_file_action
        for c in model.categories:
            self._categories.__add__([c])

        for level in model.security_levels:
            self._security_levels.add_item(level)

    def create_file_action(self):
        indexes = self._categories.selected_rows_indexes
        cats = []
        for index in indexes:
            cats.append(self._categories.value[index][0])

        file_categories = cats
        user_categories = self.model.user_categories[self.user]

        file_security = self.model.security_levels[self._security_levels.value]
        user_security = self.model.security_levels[self.model.user_security[self.user]]

        if user_security <= file_security and all(c in file_categories for c in user_categories):
            self.model.add_file_action(self._filename.value, cats, self._security_levels.value, self._content.value)
            self.close()
        elif user_security > file_security:
            self.alert('File security is too low')
        else:
            self.alert('File categories is not a superset of user categories')

class EditUserWindow(BaseWidget):
    def __init__(self, model, username):
        self.username = username
        super(EditUserWindow, self).__init__('Edit {}'.format(username))
        self.model = model
        # Definition of the forms fields
        self._categories = ControlList(label='Categories', readonly=True)
        self._security_levels = ControlCombo(label='Security Level')
        self._button = ControlButton('Edit User')

        self._button.value = self.edit_user_action
        for c in model.categories:
            self._categories.__add__([c])

        for level in model.security_levels:
            self._security_levels.add_item(level)

    def edit_user_action(self):
        indexes = self._categories.selected_rows_indexes
        cats = []
        for index in indexes:
            cats.append(self._categories.value[index][0])
        self.model.edit_user_action(self.username, cats, self._security_levels.value)
        self.close()


class ShowFileWindow(BaseWidget):
    def __init__(self, model, filename, write: bool):
        self._filename = filename
        super(ShowFileWindow, self).__init__(filename)
        self.model = model

        self._content = ControlTextArea(readonly=not write)
        if write:
            self._content.value = ''
        else:
            self._content.value = self.model.file_contents[filename]

        self._save_button = ControlButton('Save', enabled=write)
        self._save_button.value = self.save

        self._cancel_button = ControlButton('Cancel')
        self._cancel_button.value = self.close

        self._formset = ['', '||', '',
                         '_content',
                         ('_save_button', '_cancel_button'),
                         '', '||', '']

    def save(self):
        self.model.file_contents[self._filename] = self._content.value
        self.close()




if __name__ == '__main__':
    pyforms.start_app(BellLaPadula)
